$fn=100;
cleanup=.001;
big_num=100;

round=1.7;
clear=.3;

usb_stickout=2.5;
usb_height=7.5+clear;
usb_offset=[3.7,3.15];
usb_cutout=[10.7,5.7];
usb_radius=1.25;
usb_mount=18.5;
usb_screw=2.5+clear;

piezo_cutout=15;

pcb=[32+.2+clear,1.6+clear,32+clear];

battery_clear=1;
battery=[30.5+clear,5+battery_clear,31.5+clear];

inside=[battery.x,usb_height+pcb.y+battery.y,pcb.z];

wall=2;
inset_d=3.2;
inset_h=4.8;
outside=[inside.x+wall*3+inset_d*2,inside.y+wall,inside.z+usb_stickout];
lid_screw=2+clear*2;
lid_screw_top=4;

sidewall=(outside.x-pcb.x)/2;
screwy=outside.y/2-inset_d/2-wall;
screwx=((inside.x+inset_d)/2+wall);

band_holder_diam=4;
band_holder_depth=2.5;
band_width=24+clear;
band_pin=1+clear;
band_holder_width=outside.x-screwx*2;

seal_thick=1;
button_height=1.65;
button_pretrusion=1;
cap_height=3;
cap_diameter=6.5;
button_offset=[11,(inside.y/2)-(usb_height+pcb.y/2)+button_height];
