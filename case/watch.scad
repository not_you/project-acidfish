include<parameters.scad>
echo(outside);
echo(inside);

thing=2;
if(thing==1){
	difference(){
		part(1,clear);
		part(0,-clear);
	}
}
else if(thing == 0){
	part(0,clear);
}
else{
	part(1,0);
}

module part(part,tollerance){
	difference(){
		union(){
			minkowski(){
				cube([outside.x-round*2,outside.y-round*2,outside.z],center=true);
				cylinder(r=round,h=cleanup/2,$fn=4);
			}
			translate([0,-outside.y/2+band_holder_diam/2,-outside.z/2])
			{
				difference(){
					for(i=[-1,1]){
						translate([(band_width+band_holder_diam)/2*i,0,0])
						{
							cube([band_holder_diam,band_holder_diam,band_holder_depth*2],center=true);
							translate([0,0,-band_holder_depth])
							rotate([0,90,0]){
								cylinder(d=band_holder_diam,h=band_holder_diam,center=true);
							}
						}
					}
					translate([0,0,-band_holder_depth])
					rotate([0,90,0])
					cylinder(d=band_pin,h=big_num,center=true);

				}
			}
		}
		translate([0,0,usb_stickout/2+cleanup]){
			cube(inside,center=true);
			translate([0,(inside.y/2)-(usb_height+pcb.y/2),0]){
				cube(pcb,center=true);
				if ( part == 0 ){
					difference(){
						translate([0,-pcb.y/2+outside.y/2,-tollerance/4])
						cube([outside.x-sidewall+tollerance,outside.y,outside.z+cleanup*5+tollerance/2],center=true);

						translate([usb_offset.x,0,0])
						union(){
							cube([usb_cutout.x+wall,big_num,big_num],center=true);
						}
					}
				}
				translate([usb_offset.x,usb_offset.y+pcb.y/2,-outside.z/2]){
					if ( part == 0 ){
						translate([ -big_num/2, usb_cutout.y/2-usb_radius, -big_num/2-tollerance/2 ])
						cube([big_num,big_num,big_num]);

					}
					
					minkowski(){
						cylinder(r=usb_radius,h=cleanup/2);
						cube([usb_cutout.x-usb_radius*2,usb_cutout.y-usb_radius*2,usb_stickout+cleanup*3],center=true);

					}
					for(i=[-1,1]){
						translate([usb_mount/2*i,0,0]){
							/*if ( part == 0 ){
								translate([0,big_num/2,0])
								cube([usb_screw,big_num,big_num],center=true);
							}*/
							cylinder(d=usb_screw,h=usb_stickout+cleanup*3,center=true);
						}
					}
				}
			}
				
		}
		for(i=[-1,1]){
			for(j=[-1,1]){
				translate([screwx*i,j*screwy,0]){
					cylinder(h=100,d=lid_screw,center=true);
					translate([0,0,-outside.z/2]){
						cylinder(d=inset_d,h=inset_h);
						cylinder(d1=inset_d+1,d2=inset_d,h=1);
					}
				}
				
			}
		}
			if( part==0){
				translate([-big_num/2,screwy-inset_d/2-wall,-big_num/2])
				cube([big_num,big_num,big_num]);
			}
			
	}
}
