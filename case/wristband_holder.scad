include<parameters.scad>

difference(){
	for(i=[-1,1]){
		translate([screwx*i,0,0])
		{
			difference(){
				union(){
					translate([i*((outside.x/2-screwx)-band_holder_width/2),0,0])
					
					minkowski(){
						cube([band_holder_width-round*2,outside.y-round*2,band_holder_depth+band_holder_diam/2],center=true);
						cylinder(r=round,h=cleanup/2,$fn=4);
					}
					
					translate([i*-(outside.x/4-round/2),-outside.y/2+band_holder_diam/2])
					cube([outside.x/2-round*2,band_holder_diam,band_holder_depth+band_holder_diam/2],center=true);
						
				}
				for(j=[-1,1]){
				
					translate([0,j*screwy,0]){
						cylinder(h=big_num,d=lid_screw-clear,center=true);
						translate([0,0,40-(band_holder_depth+outside.z+seal_thick+1)])
						cylinder(h=big_num,d=4);

					}
				}
				
			}
		}
	}
	union(){
		translate([0,-outside.y/2+band_holder_diam/2])
		{
			cube([band_width,band_holder_diam+cleanup,band_holder_depth+band_holder_diam/2],center=true);
			translate([0,0,band_holder_depth/4])
			rotate([0,90,0])
			cylinder(d=band_pin,h=band_width+6,center=true);
		}
	}
}
