include<parameters.scad>

part=0;
/*
if(part==1){
    difference(){
        translate([0,0,3])
        cube([60,30,6],center=true);
        translate([0,0,-seal_thick/2])
        model();
        for(i=[-1,1]){
             for(j=[-1,1]){
                translate([((inside.x+inset_d)/2+wall)*i,j*(inside.y/2-lid_screw/2),0])
                cylinder(h=8,d=lid_screw,center=true);
                    
            }
        }
    }
}
else{
    difference(){
        union(){
            translate([0,0,-3])
            cube([60,30,6],center=true);
            
        }
        translate([0,0,-seal_thick/2])
        model();
        
    }
    for(i=[-1,1]){
         for(j=[-1,1]){
             translate([((inside.x+inset_d)/2+wall)*i,j*(inside.y/2-lid_screw/2),8])
             cylinder(h=8,d=lid_screw-.2,center=true);
                        
         }
     }
}
*/
model2();

 
module model(){
difference(){
        union(){
            minkowski(){
                cube([outside.x-round*2,outside.y-round*2,seal_thick],center=true);
                cylinder(d=5,h=cleanup/2);
            }
            for(i=[-1,0,1]){
                echo(button_offset.y);
                translate([button_offset.x*i,button_offset.y,seal_thick/2])
                {
                    cylinder(h=button_pretrusion,d=cap_diameter);
                    cylinder(h=cap_height,d=3);
                }
            }
        }
        for(i=[-1,0,1]){
            translate([button_offset.x*i,button_offset.y,-button_pretrusion/2-cleanup])
            cylinder(h=button_pretrusion,d=cap_diameter-seal_thick);
        }
        for(i=[-1,1]){
            for(j=[-1,1]){
                translate([((inside.x+inset_d)/2+wall)*i,j*(inside.y/2-lid_screw/2),0])
                cylinder(h=100,d=lid_screw,center=true);
                
            }
        }
    }
};

module model2(){
difference(){
        union(){
            minkowski(){
                cube([outside.x-round*2,outside.y-round*2,5],center=true);
                cylinder(d=5,h=cleanup/2);
            }
            /*for(i=[-1,0,1]){
                echo(button_offset.y);
                translate([button_offset.x*i,button_offset.y,seal_thick/2])
                {
                    cylinder(h=button_pretrusion,d=cap_diameter);
                    cylinder(h=cap_height,d=3);
                }
            }*/
        }
        for(i=[-1,0,1]){
            translate([button_offset.x*i,button_offset.y,-button_pretrusion-cleanup])
            cylinder(h=10,d=cap_diameter+1,center=true);
        }
        for(i=[-1,1]){
            for(j=[-1,1]){
                translate([((inside.x+inset_d)/2+wall)*i,j*(inside.y/2-lid_screw/2),0])
                cylinder(h=100,d=lid_screw,center=true);
                
            }
        }
    }
};